# Simple python3 script to import vtk files from OpenFOAM runs and export
# png screenshots for animation
from paraview.simple import *
import os

camera=GetActiveCamera()
camera.SetFocalPoint(0.2,0,0)
camera.SetPosition(0.2,0,1)
camera.SetViewUp(0,1,0)
camera.Dolly(2.0)
##Show()
##Render()

#### disable automatic camera reset on 'Show'
#paraview.simple._DisableFirstRenderCameraReset()

# Set up an ordered list of the files
# You will need to change this path to point to where the vtk files are!
basepath = '/home/grtabor/OpenFOAM/grtabor-7/run/OF15TurbTraining/bfStepLES/postProcessing/cuttingPlane'  
listFiles = os.listdir(basepath)
listDirectories = []

for fname in listFiles:
    path = os.path.join(basepath, fname)
    if os.path.isdir(path):
        listDirectories.append(fname)

sortedListDirectories = sorted(listDirectories,key=float)
print(sortedListDirectories)

ii = 0
for fname in sortedListDirectories:
    VTKFile = os.path.join(basepath,fname+'/U_zNormal.vtk')
    print(VTKFile)

    BFSClip=OpenDataFile(VTKFile)

    # get color transfer function/color map for 'U'
    uLUT = GetColorTransferFunction('U')

    # get active view
    renderView1 = GetActiveViewOrCreate('RenderView')
    # uncomment following to set a specific view size
    # renderView1.ViewSize = [843, 546]

    # Hide the scalar bar for this color map if no visible data is colored by it.
    #HideScalarBarIfNotNeeded(uLUT, renderView1)

    # get active source.
    legacyVTKReader1 = GetActiveSource()

    # get display properties
    legacyVTKReader1Display = GetDisplayProperties(legacyVTKReader1, view=renderView1)

    # set scalar coloring
    ColorBy(legacyVTKReader1Display, ('POINTS', 'U', 'Magnitude'))

    # rescale color and/or opacity maps used to include current data range
    legacyVTKReader1Display.RescaleTransferFunctionToDataRange(True, False)

    # show color bar/color legend
    legacyVTKReader1Display.SetScalarBarVisibility(renderView1, True)

    Show()
    Render()

    # save screenshot
    SaveScreenshot('image'+str(ii).zfill(4)+'.png', renderView1, ImageResolution=[843, 546])
    ii += 1

    # destroy legacyVTKReader1
    Delete(legacyVTKReader1)
    del legacyVTKReader1
